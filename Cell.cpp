#include "Cell.h"

Road::Road(int x, int y, int s) 
{
	xPos = x;
	yPos = y;
	size = s;
};

void Road::render(sf::RenderWindow& window)
{
	shape.setFillColor(sf::Color(0, 0, 0));
	shape.setSize(sf::Vector2f(size, size));
	shape.setPosition(sf::Vector2f(xPos, yPos));
	window.draw(shape);
};

Wall::Wall(int x, int y, int s) 
{
	xPos = x;
	yPos = y;
	size = s;
};

void Wall::render(sf::RenderWindow& window)
{
	shape.setFillColor(sf::Color(225, 93, 199));
	shape.setSize(sf::Vector2f(size, size));
	shape.setPosition(sf::Vector2f(xPos, yPos));
	window.draw(shape);
};

static sf::FloatRect moveRect(const sf::FloatRect& rect, sf::Vector2f& offset)
{
	return { rect.left + offset.x, rect.top + offset.y, rect.width, rect.height };
}

//bool isWall(const Cell& cells, const sf::FloatRect& oldBounds, sf::Vector2f& movement)
//{
//	sf::FloatRect newBounds = moveRect(oldBounds, movement);
//	bool changed = false;
//	for (auto cell : cells.cells)
//	{
//
//	}
//
//}