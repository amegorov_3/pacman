#pragma once
#include "Entity.h"
#include<iostream>

//enum struct Cells
//{
//	ROAD,
//	WALL
//};



class Cell : public StaticEntity
{
public:
	Collisions collisionType;
	//std::vector<Cell*> cells;
	//int length = 32;
	//int width = 24;
	/*Cell(int x, int y, int s) {
		xPos = x;
		yPos = y;
		size = s;
	}*/
	virtual void render(sf::RenderWindow& window) = 0;
	//{
	//	shape.setFillColor(sf::Color(52, 93, 199));
	//	shape.setSize(sf::Vector2f(size, size));
	//	shape.setPosition(sf::Vector2f(xPos, yPos));
	//	window.draw(shape);
	//}
};

class Road : public Cell
{
public:
	Collisions collisionType = Collisions::NONE;
	Road(int x, int y, int s);/* {
		xPos = x;
		yPos = y;
		size = s;
	};*/
	void render(sf::RenderWindow& window);
	//{
	//	shape.setFillColor(sf::Color(0, 0, 0));
	//	shape.setSize(sf::Vector2f(size, size));
	//	shape.setPosition(sf::Vector2f(xPos, yPos));
	//	window.draw(shape);
	//};

	//Cells getCategory() { return celCategory; };

};

class Wall : public Cell
{
public:
	Collisions collisionType = Collisions::WALL;
	Wall(int x, int y, int s);/*{
		xPos = x;
		yPos = y;
		size = s;
	};*/
	void render(sf::RenderWindow& window);
	//{
	//	shape.setFillColor(sf::Color(225, 93, 199));
	//	shape.setSize(sf::Vector2f(size, size));
	//	shape.setPosition(sf::Vector2f(xPos, yPos));
	//	window.draw(shape);
	//};
};