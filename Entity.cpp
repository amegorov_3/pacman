#include "Entity.h"

PacGum::PacGum(int x, int y, int s)
{
	xPos = x;
	yPos = y;
	size = s;
};

void PacGum::render(sf::RenderWindow& window) {
	gums_shape.setFillColor(sf::Color(148, 0, 211));
	gums_shape.setRadius(size / 2);
	gums_shape.setPosition(sf::Vector2f(xPos + 20 - size/2 , yPos + 20 - size/2));
	window.draw(gums_shape);
};

SuperPacGum::SuperPacGum(int x, int y, int s)
{
	xPos = x;
	yPos = y;
	size = s;
};

void SuperPacGum::render(sf::RenderWindow& window) {
	//std::cout << "SUPER GUM" << std::endl;
	gums_shape.setOutlineColor(sf::Color(255, 255, 255));
	gums_shape.setOutlineThickness(1);
	gums_shape.setFillColor(sf::Color(85, 85, 255));
	gums_shape.setRadius(size / 2);
	gums_shape.setPosition(sf::Vector2f(xPos + 20 - size / 2, yPos + 20 - size / 2));
	window.draw(gums_shape);
};
//sf::FloatRect PacGum::getGumPosition(PacGum* p)
//{
//	return sf::FloatRect(p->xPos, p->yPos, p->size, p->size);
//}
//bool PacGum::eaten(Pacman& pacman, sf::Vector2f movement)
//{
//	return true;
//};