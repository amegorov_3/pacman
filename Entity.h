#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>

enum struct Direction
{
	NONE,
	UP,
	DOWN,
	LEFT,
	RIGHT
};

enum struct  Collisions
{
	NONE,
	WALL,
	GHOST,
	GUM,
	SUPERGUM
};

class Entity
{
public:
	Collisions collisionType;
protected:
	int xPos;
	int yPos;
	int size;


public:
	virtual sf::FloatRect getBounds() = 0;
	//virtual void update() = 0;
	virtual void render(sf::RenderWindow& window) = 0;
};

class StaticEntity : public Entity {
protected:
	sf::FloatRect bounds;
	sf::RectangleShape shape;
	sf::CircleShape gums_shape;
	//sf::CircleShape supergum_shape;
public:
	Collisions collisionType;
	sf::FloatRect getBounds() override {
		return shape.getGlobalBounds();
	}
};

class PacGum : public StaticEntity
{

public:

	//sf::FloatRect getGumPosition(PacGum* p);
	Collisions collisionType = Collisions::GUM;
	PacGum(int x, int y, int s);/* {
		xPos = x;
		yPos = y;
		size = s;

	}*/
	sf::FloatRect getBounds()  {
		return gums_shape.getGlobalBounds();
	}
	void render(sf::RenderWindow& window); /*{
		gum_shape.setFillColor(sf::Color(255, 183, 174));
		gum_shape.setRadius(size / 2);
		gum_shape.setPosition(sf::Vector2f(xPos, yPos));
	}*/
	//	bool eaten(Pacman& pacman, sf::Vector2f movement);
};




class SuperPacGum : public StaticEntity
{
public:
	Collisions collisionType = Collisions::SUPERGUM;
	SuperPacGum(int x, int y, int s); //{
		//xPos = x;
		//yPos = y;
		//size = s;
	//}
	//sf::FloatRect getBounds() override {
	//	return supergum_shape.getGlobalBounds();
	sf::FloatRect getBounds() {
		return gums_shape.getGlobalBounds();
	}
	void render(sf::RenderWindow& window); //{
		//shape.setFillColor(sf::Color(255, 183, 174));
		//shape.setSize(sf::Vector2f(size, size));
		//shape.setPosition(sf::Vector2f(xPos, yPos));

	//}
};


class MovingEntity : public Entity {
protected:
	int speed;
	sf::Vector2i speedVec;
	Direction direction;

	//etc.

public:
	sf::CircleShape shape;
	void unpdatePosition() {

	}
	void render(sf::RenderWindow& window) {
		window.draw(shape);
	}
	sf::FloatRect getBounds() {
		return shape.getGlobalBounds();
	}
	//setters, getters, etc. 
};