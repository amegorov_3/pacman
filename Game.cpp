#include "Game.h"
#include <iostream>
#include <fstream>
#include <string>


Game::Game() {
    
    panel = new UIPanel();
    createMaze();
    gameState = new Active;

    // create factory AbstractGhostFactory* ghostFactory;
    // read the text file with maze and positions of entities
    //if x - create wall and add into walls
    //if P - create pacman, add into objects
    //if b/p/i/c - create factories
    //create ghosts and add them into objects and ghosts
    //if . - create PacGum into objects
    //if o - create SuperPacGum
    //if - - create GhostHouse 
    //pacman = new Pacman(window_length / length + 5, window_width / width + 5);
    //pacman->initializePacman();
}


void  Game::render(sf::RenderWindow& window) const {
    
    for (auto cell : cells) {
        cell->render(window);
    }

    for (auto pacgum : objects)
    {
        pacgum->render(window);

    }
    for (auto ghost : ghosts)
    {
        ghost->render(window);
    }
    pacman->render(window);
    panel->render(window);
    gameState->draw(window);

}


void Game::updateGame(float elapsedTime) {
    eaten(pacman);
    if (!gameState->update(*this, elapsedTime))
        this->updateState(new GameOver);

    //sf::Vector2f try_move = pacman->updatePacman(elapsedTime);

    //Collisions pacman_collision = isCollision(pacman, try_move);
    //sf::Vector2f real_pacman_move(0.f, 0.f);
    //eaten(pacman);

    //switch (pacman_collision)
    //{
    //case Collisions::WALL:
    //    //std::cout << "-" << std::endl;
    //    break;

    //case Collisions::NONE:
    //    real_pacman_move = try_move;
    //    //std::cout << "+" << std::endl;
    //    break;
    //    //case Collisions::GUM:
    //    //    eaten(pacman);
    //    //    real_move = try_move;


    //};
    //for (auto ghost : ghosts)
    //{
    //    sf::Vector2f ghost_try_move = ghost->expected_movement(elapsedTime, pacman, real_pacman_move);
    //    Collisions ghost_collision = isCollision(ghost, ghost_try_move);
    //    sf::Vector2f real_ghost_move(0.f, 0.f);
    //    
    //    switch (ghost_collision)
    //    {
    //    case Collisions::WALL:
    //        //std::cout << "-" << std::endl;
    //        //ghost->update(elapsedTime, pacman,  sf::Vector2f(try_move.x * (-1), try_move.y * (-1)));
    //        break;

    //    case Collisions::NONE:
    //        real_ghost_move = ghost_try_move;
    //        
    //        ghost->update(elapsedTime, pacman, real_pacman_move);
    //        //std::cout << "+" << std::endl;
    //        break;
    //        //case Collisions::GUM:
    //        //    eaten(pacman);
    //        //    real_move = try_move;


    //    };
    //    //ghost->update(elapsedTime, pacman, real_pacman_move);

    //    if (ghost->getBounds().intersects(pacman->getBounds()))
    //    {
    //        //return current_collision;
    //    }
    //}
    //pacman->shape.move(real_pacman_move);


}

Collisions Game::isCollision(MovingEntity* entity, sf::Vector2f& movement)
{
    Collisions current_collision = Collisions::NONE;
    sf::FloatRect current_location = entity->getBounds();
    float x = current_location.left;
    float y = current_location.top;
    sf::FloatRect potential_location(x + movement.x, y + movement.y, current_location.height, current_location.width);

    for (auto cell : cells)
    {

        float cell_x = cell->getBounds().left;
        float cell_y = cell->getBounds().top;
        float cell_height = cell->getBounds().height;
        float cell_width = cell->getBounds().width;

        sf::FloatRect cell_location(cell_x, cell_y, cell_height, cell_width);

        if (cell->getBounds().intersects(potential_location) && cell->collisionType == Collisions::WALL)
        {
            current_collision = cell->collisionType;

            //std::cout << "INTERSECTION" << std::endl;
            return current_collision;
        }


    };

    return current_collision;
}


void Game::createMaze() {
    abstractGhostFactory* Pfactory = new PinkyFactory;
    abstractGhostFactory* Bfactory = new BlinkyFactory;
    abstractGhostFactory* Ifactory = new InkyFactory;
    abstractGhostFactory* Cfactory = new ClydeFactory;
    std::string maze[24] = { "################################",
                             "#c............................p#",
                             "######.#.#..#.#..#.#..# #.######",
                             "#...*#.#.##.#.#..#.#.##.#.#*...#",
                             "#.####.#.#..#.####.#..#.#.####.#",
                             "#......#.#.##.#..#.##.#.#......#",
                             "##.#####................#####.##",
                             "#.........#.##.##.##.#.........#",
                             "#.#.#####.#.#..##..#.#.#####.#.#",
                             "#.#.......#.#......#.#.......#.#",
                             "#.#.#.#####.###.P###.#####.#.#.#",
                             "#.#.#.#...#...#..#...#...#.#.#.#",
                             "#.#.#...#...#......#...#...#.#.#",
                             "#.#.#.#####.###..###.#####.#.#.#",
                             "#.#       # #      # #       # #",
                             "#.# ##### # #  ##  # # ##### # #",
                             "#.        # ## ## ## #         #",
                             "##.#####                ##### ##",
                             "#.     # # ## #  # ## # #      #",
                             "#.#### # #  # #### #  # # #### #",
                             "#.  *# # ## # #  # # ## # #*   #",
                             "###### # #  # #  # #  # # ######",
                             "#b............................i#",
                             "################################" };
    cells.reserve(length * width);
    ghosts.reserve(4);
    for (size_t y = 0; y < width; y++) {
        for (size_t x = 0; x < length; x++) {
            if (maze[y][x] == 'P')
            {
                pacman = new Pacman(x * window_length / length, y * window_width / width, 36);
            }
            else if (maze[y][x] == '#')
            {
                Cell* cell = new Wall(x * window_length / length, y * window_width / width, 40);
                cell->collisionType = Collisions::WALL;

                cells.push_back(cell);
                //objects.push_back(cells.back());
            }
            else if (maze[y][x] == ' ')
            {
                Cell* cell = new Road(x * window_length / length, y * window_width / width, 40);
                cell->collisionType = Collisions::NONE;
                cells.push_back(cell);
            }
            else if (maze[y][x] == '.')
            {
                Entity* pacgum = new PacGum(x * window_length / length, y * window_width / width, 16);
                pacgum->collisionType = Collisions::GUM;
                objects.push_back(pacgum);
            }
            else if (maze[y][x] == '*')
            {
                Entity* superpacgum = new SuperPacGum(x * window_length / length, y * window_width / width, 20);
                superpacgum->collisionType = Collisions::SUPERGUM;
                objects.push_back(superpacgum);
            }
            else if (maze[y][x] == 'b')
            {
                //Ghost* blinky = Bfactory->createGhost(x * 50, y * 50);
                //std::cout << blinky->getPosition().x << '\t' << blinky->getBounds().top << std::endl;
                //blinky->setPosition(x, y);
                //blinky->collisionType = Collisions::GHOST;
                ghosts.push_back(Bfactory->createGhost(x * window_length / length, y * window_width / width));
                std::cout << ghosts.size();
                //objects.push_back(ghosts.back());
            }
            else if (maze[y][x] == 'i')
            {
                //Ghost* blinky = Bfactory->createGhost(x * 50, y * 50);
                //std::cout << blinky->getPosition().x << '\t' << blinky->getBounds().top << std::endl;
                //blinky->setPosition(x, y);
                //blinky->collisionType = Collisions::GHOST;
                ghosts.push_back(Ifactory->createGhost(x * window_length / length, y * window_width / width));
                std::cout << ghosts.size();
                //objects.push_back(ghosts.back());
            }
            else if (maze[y][x] == 'c')
            {
                //Ghost* blinky = Bfactory->createGhost(x * 50, y * 50);
                //std::cout << blinky->getPosition().x << '\t' << blinky->getBounds().top << std::endl;
                //blinky->setPosition(x, y);
                //blinky->collisionType = Collisions::GHOST;
                ghosts.push_back(Cfactory->createGhost(x * window_length / length, y * window_width / width));
                std::cout << ghosts.size();
                //objects.push_back(ghosts.back());
            }
            else if (maze[y][x] == 'p')
            {
                //Ghost* blinky = Bfactory->createGhost(x * 50, y * 50);
                //std::cout << blinky->getPosition().x << '\t' << blinky->getBounds().top << std::endl;
                //blinky->setPosition(x, y);
                //blinky->collisionType = Collisions::GHOST;
                ghosts.push_back(Pfactory->createGhost(x * window_length / length, y * window_width / width));
                std::cout << ghosts.size();
                //objects.push_back(ghosts.back());
            }
        }
    }
    delete Pfactory;
    delete Cfactory;
    delete Ifactory;
    delete Bfactory;
}


bool Game::eaten(Pacman* pacman)
{

    for (auto it = objects.begin(); it != objects.end(); it++)
    {
        //std::cout << (*it)->getBounds().left << "\t" << (**it).getBounds().top << '\t' << (**it).getBounds().height << '\t' << (**it).getBounds().width <<  std::endl;
        if (pacman->getBounds().intersects((*it)->getBounds()))
        {
            //std::string type = typeid(*(*it)).name();
            //if (type == "class PacGum")
            if ((*it)->collisionType == Collisions::GUM)
            {
                std::cout << "GUM" << std::endl;
                objects.erase(it);
                panel->updateTotal(10);
                return true;
            }
            else if ((*it)->collisionType == Collisions::SUPERGUM)
            {
                std::cout << "SUPERGUM" << std::endl;
                objects.erase(it);
                panel->updateTotal(100);
                return true;
            }
        }
    }
}
