#pragma once
//#pragma warning(disable : 4996)


#define window_width 960
#define window_length 1280

//#include <vector>
#include <SFML/Graphics.hpp>
#include "Pacman.h"
#include "Entity.h"
#include "Ghost.h"
#include "Cell.h"
#include "UIPanel.h"



class GameState;

class Active;

class GameOver;

class Game
{
public:
	int length = 32;
	int width = 24;
	std::vector<Entity*> objects;
	std::vector<Ghost*> ghosts;
	std::vector<Cell*> cells;
	UIPanel* panel;
	Pacman* pacman;
	Collisions isCollision(MovingEntity* pacman, sf::Vector2f& movement);
	GameState* gameState;
	bool eaten(Pacman* pacman);
	void updateState(GameState* newState) {
		delete gameState;
		gameState = newState;
	}
	~Game()
	{
		objects.clear();
		ghosts.clear();
		cells.clear();
	}

	Game();
	//std::vector<Entity> getEntities();
	//std::vector<Cell> getWalls();
	Pacman getPacman();


	void createMaze();
	void updateGame(float elapsedTime); //   
	void render(sf::RenderWindow& window) const;
	//sf::Vector2f normalized(sf::Vector2f a)
	//{
	//	float new_x = a.x / sqrt(a.x * a.x + a.y * a.y);
	//	float new_y = a.y / sqrt(a.x * a.x + a.y * a.y);
	//	return sf::Vector2f(new_x, new_y);
	//};// 
	//~Game() = default;

};

//bool eaten(Pacman& pacman);


class GameState {
public:
	virtual bool update(Game& game, float elapsedTime) = 0;
	virtual void draw(sf::RenderWindow& window) = 0;
};


class GameOver : public GameState {
public:
	bool update(Game& game, float elapsedTime) override {
		return false;
	}
	void draw(sf::RenderWindow& window) override {
		sf::Font font;
		font.loadFromFile("Emulogic-zrEw.ttf");
		sf::Text text;
		text.setFont(font);
		text.setString("Game over");
		text.setCharacterSize(50);
		text.setFillColor(sf::Color::White);
		text.setStyle(sf::Text::Bold);
		text.setPosition(400, 400);
		window.draw(text);

	}
};

class Active : public GameState {
public:
	bool update(Game& game, float elapsedTime) override
	{
		sf::Vector2f try_move = game.pacman->updatePacman(elapsedTime);

		Collisions pacman_collision = game.isCollision(game.pacman, try_move);
		sf::Vector2f real_pacman_move(0.f, 0.f);
		switch (pacman_collision)
		{
		case Collisions::WALL:
			//std::cout << "-" << std::endl;
			break;

		case Collisions::NONE:
			real_pacman_move = try_move;
			//std::cout << "+" << std::endl;
			break;
			//case Collisions::GUM:
			//    eaten(pacman);
			//    real_move = try_move;


		};
		game.pacman->shape.move(real_pacman_move);
		for (auto ghost : game.ghosts)
		{
			sf::Vector2f ghost_try_move = ghost->expected_movement(elapsedTime, game.pacman, real_pacman_move);
			Collisions ghost_collision = game.isCollision(ghost, ghost_try_move);
			sf::Vector2f real_ghost_move(0.f, 0.f);

			switch (ghost_collision)
			{
			case Collisions::WALL:
				//std::cout << "-" << std::endl;
				//ghost->update(elapsedTime, pacman,  sf::Vector2f(try_move.x * (-1), try_move.y * (-1)));
				break;

			case Collisions::NONE:
				real_ghost_move = ghost_try_move;

				ghost->update(elapsedTime, game.pacman, real_pacman_move);
				//std::cout << "+" << std::endl;
				break;
				//case Collisions::GUM:
				//    eaten(pacman);
				//    real_move = try_move;
			};
			//ghost->update(elapsedTime, pacman, real_pacman_move);

			if (ghost->getBounds().intersects(game.pacman->getBounds()))
			{

				return false;
			}
			return true;
		}
		
	}
	void draw(sf::RenderWindow& window) override
	{

	}
};
