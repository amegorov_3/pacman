#pragma once
#include <iostream>
#include "Entity.h"
#include "Pacman.h"
#include <math.h>

//static const sf::Color BLINKY_COLOR = sf::Color(255, 216, 0);
static const float GHOST_SPEED = 120.f; // pixels per second.
//static const float GHOST_RADIUS = 16.f;


class Ghost;

class GhostStrategy {
public:
	virtual sf::Vector2f chaseStrategy(Ghost* ghost, Pacman* pacman, sf::Vector2f pacman_movement) = 0;
	virtual sf::Vector2f scatterStrategy(Ghost* ghost) = 0;
	sf::Vector2f normalized(sf::Vector2f a)
	{
		float new_x = a.x / sqrt(a.x * a.x + a.y * a.y);
		float new_y = a.y / sqrt(a.x * a.x + a.y * a.y);
		return sf::Vector2f(new_x, new_y);
	};
	virtual ~GhostStrategy() = default;
};


class Ghost : public MovingEntity
{
protected:
	sf::Texture texture;
	sf::Sprite sprite;
	float speed;
	//GhostState* state;
	sf::Vector2i home;
	GhostStrategy* strategy;
public:
	Collisions collisionType = Collisions::GHOST;

	void setPosition(float x, float y) {
		sprite.setPosition(x, y);
	}
	void render(sf::RenderWindow& window) {
		//std::cout << "BLINKY" << std::endl;
		window.draw(sprite);
	}
	sf::Vector2f expected_movement(float elapsedTime, Pacman* pacman, sf::Vector2f expected_pacman_movement)
	{
		const float step = speed * elapsedTime;
		sf::Vector2f expected_movement = sf::Vector2f(strategy->chaseStrategy(this, pacman, expected_pacman_movement).x * step, strategy->chaseStrategy(this, pacman, expected_pacman_movement).y * step);
		return expected_movement;
	};
	void update(float elapsedTime, Pacman* pacman, sf::Vector2f movement) {
		
		sprite.move(expected_movement(elapsedTime, pacman, movement));

	}
	sf::FloatRect getBounds() {
		return sprite.getGlobalBounds();
	}
	/*void setState(GhostState* newState) {
		delete state;
		state = newState;
	}*/
	sf::Vector2f getPosition() {
		return sprite.getPosition();
	}

	GhostStrategy* getStrategy() {
		return strategy;
	}

	~Ghost() {
		//delete state;
		//delete strategy;
	}

};


class BlinkyStrategy : public GhostStrategy {
public:
	sf::Vector2f chaseStrategy(Ghost* ghost, Pacman* pacman, sf::Vector2f pacman_movement) override
	{
		sf::Vector2f ghost_position = sf::Vector2f(ghost->getPosition().x, ghost->getPosition().y);
		sf::Vector2f ghost_movement = normalized(sf::Vector2f(pacman->getBounds().left + pacman_movement.x - ghost_position.x, pacman->getBounds().top + pacman_movement.y - ghost_position.y));
		//pacman->shape.move(real_move);
		return ghost_movement;
	}

	sf::Vector2f scatterStrategy(Ghost* ghost) override {
		return sf::Vector2f(0.f, 0.f);
		// Implement Blinky's scatter strategy.
	}
};

//class PinkyStrategy : public GhostStrategy {
//public:
//	sf::Vector2f chaseStrategy(Ghost* ghost, Pacman* pacman, sf::Vector2f pacman_movement) override {
//		// Implement Pinky's chase strategy.
//	}
//
//	sf::Vector2f scatterStrategy(Ghost* ghost) override {
//		// Implement Pinky's scatter strategy.
//	}
//};
//
//class ClydeStrategy : public GhostStrategy {
//public:
//	sf::Vector2f chaseStrategy(Ghost* ghost, Pacman* pacman, sf::Vector2f pacman_movement) override {
//		// Implement Clyde's chase strategy.
//	}
//
//	sf::Vector2f scatterStrategy(Ghost* ghost) override {
//		// Implement Clyde's scatter strategy.
//	}
//};
//
//class InkyStrategy : public GhostStrategy
//{
//public:
//	sf::Vector2f chaseStrategy(Ghost* ghost, Pacman* pacman, sf::Vector2f pacman_movement) override {
//		// Implement Inky's chase strategy.
//	};
//
//	sf::Vector2f scatterStrategy(Ghost* ghost) override {
//		// Implement Inky's scatter strategy.
//	};
//};

class Blinky : public Ghost {
public:
	Blinky(int xPos, int yPos) {
		strategy = new BlinkyStrategy;
		collisionType = Collisions::GHOST;
		sprite.setPosition((xPos)*1.0, (yPos + 1) * 1.0);
		texture.loadFromFile("Blinky.png");
		sprite.setTexture(texture);
		sprite.setScale(0.05, 0.05);
		speed = 20;

	}

};
class Clyde : public Ghost {
public:
	Clyde(int xPos, int yPos) {
		/*strategy = new ClydeStrategy;*/
		strategy = new BlinkyStrategy;
		collisionType = Collisions::GHOST;
		sprite.setPosition((xPos)*1.0, (yPos + 2)*1.0);
		texture.loadFromFile("Clyde.png");
		sprite.setTexture(texture);
		sprite.setScale(0.05, 0.05);
		speed = 40;
		//state = new HouseMode;

	}
};

class Pinky : public Ghost {
public:
	Pinky(int xPos, int yPos) {
		/*strategy = new PinkyStrategy;*/
		strategy = new BlinkyStrategy;
		collisionType = Collisions::GHOST;
		sprite.setPosition((xPos - 1)*1.0, (yPos + 10)*1.0);
		texture.loadFromFile("Pinky.png");
		sprite.setTexture(texture);
		sprite.setScale(0.03, 0.03);
		speed = 40;
		//state = new HouseMode;

	}
};

class Inky : public Ghost {
public:
	Inky(int xPos, int yPos) {
		/*strategy = new InkyStrategy;*/
		strategy = new BlinkyStrategy;
		collisionType = Collisions::GHOST;
		sprite.setPosition((xPos - 3)*1.0, (yPos + 6)*1.0);
		texture.loadFromFile("Inky.png");
		sprite.setTexture(texture);
		sprite.setScale(0.03, 0.03);
		speed = 40;
		//state = new HouseMode;

	}
};


class abstractGhostFactory
{
public:
	virtual Ghost* createGhost(int x, int y) = 0;
};

class PinkyFactory : public abstractGhostFactory
{
public:
	Ghost* createGhost(int xPos, int yPos) override {
		Ghost* ghost = new Pinky(xPos, yPos);
		//ghost->setPosition(x, y);
		return ghost;
	}
};

class InkyFactory : public abstractGhostFactory
{
public:
	Ghost* createGhost(int xPos, int yPos) override {
		abstractGhostFactory* Pfactory = new PinkyFactory;

		Ghost* ghost = new Inky(xPos, yPos);
		//ghost->setPosition(x, y);
		delete Pfactory;
		return ghost;
	}
};

class BlinkyFactory : public abstractGhostFactory
{
public:
	Ghost* createGhost(int xPos, int yPos) override {
		Ghost* ghost = new Blinky(xPos, yPos);
		//ghost->setPosition(x, y);
		return ghost;
	}
};

class ClydeFactory : public abstractGhostFactory
{
public:
	Ghost* createGhost(int xPos, int yPos) override {
		Ghost* ghost = new Clyde(xPos, yPos);
		//ghost->setPosition();
		return ghost;
	}
};

//class GhostStrategy : public Ghost
//{
//public:
//	sf::Vector2f getChasePosition()
//	{
//
//	};
//	sf::Vector2f getScatterPosition()
//	{
//
//	};
//};
//
//class BlinkyStrategy
//{
//
//};

