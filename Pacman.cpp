#include "Pacman.h"



Pacman::Pacman(int xPos, int yPos, int size)
{
    Direction direction = Direction::NONE;
    shape.setRadius(size / 2);
    shape.setFillColor(PACKMAN_COLOR);
    shape.setPosition(sf::Vector2f(xPos, yPos));
};
    
void Pacman::updatePacmanDirection() 
{
    direction = Direction::NONE;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
        || sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        direction = Direction::UP;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
        || sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        direction = Direction::DOWN;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
        || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        direction = Direction::LEFT;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)
        || sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        direction = Direction::RIGHT;
    }
}

sf::Vector2f Pacman::updatePacman(float elapsedTime) {
    const float step = PACKMAN_SPEED * elapsedTime;

    updatePacmanDirection();

    sf::Vector2f movement(0.f, 0.f);

    switch (direction)
    {
    case Direction::UP:
        movement.y -= step;
        std::cout << movement.x << '\t' << movement.y << std::endl;
        break;
    case Direction::DOWN:
        movement.y += step;
        break;
    case Direction::LEFT:
        movement.x -= step;
        break;
    case Direction::RIGHT:
        movement.x += step;
        break;
    case Direction::NONE:
        break;
    }
    const sf::FloatRect packmanBounds = shape.getGlobalBounds();
    //  if (checkFieldWallsCollision(field, packmanBounds, movement))
     // {
          //    
    //      direction = Direction::NONE;
     // }
    //coll = isCollision();
    //shape.move(movement);
    return movement;
}

//Collisions Pacman::isCollision(Pacman& pacman, sf::Vector2f& movement)
//{
//    Collisions current_collision = Collisions::NONE;
//    sf::FloatRect current_location = pacman.getBounds();
//    float x = current_location.left;
//    float y = current_location.top;
//    sf::FloatRect potential_location(x, y, current_location.height, current_location.width);
//    for (auto cell : cells)
//    {
//        float cell_x = cell->getBounds().left;
//        float cell_y = cell->getBounds().top;
//        float cell_height = cell->getBounds().height;
//        float cell_width = cell->getBounds().width;
//        sf::FloatRect cell_location(cell_x, cell_y, cell_height, cell_width);
//        if (potential_location.intersects(cell_location))
//        {
//            current_collision = cell->collisionType;
//        }
//    };
//    return current_collision;
//}