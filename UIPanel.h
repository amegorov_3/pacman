#pragma once
#include <SFML/Graphics.hpp>

static const sf::Color FONT_COLOR = sf::Color(255, 216, 0);

class UIPanel
{
private:
	sf::Font font;
	sf::Text textTotal;
	int pointTotal;
public:
	UIPanel() 
	{
		std::cout << "FONT CREATED" << std::endl;
		pointTotal = 0;
		if (!font.loadFromFile("Emulogic-zrEw.ttf")) 
		{
			throw "Could not load font";
		}
		textTotal.setFont(font);
		textTotal.setCharacterSize(26);
		textTotal.setPosition(400, 5);
		textTotal.setOutlineColor(FONT_COLOR);
		textTotal.setFillColor(FONT_COLOR);
		textTotal.setString("Score: " + std::to_string(pointTotal));
	}
	void updateTotal(int inc) {
		pointTotal += inc;
		textTotal.setString("Score: " + std::to_string(pointTotal));
	}
	void render(sf::RenderWindow& window) {
		window.draw(textTotal);
	};
};

//void UIPanel::updateTotal(int inc) {
//	pointTotal += inc;
//	textTotal.setString(std::to_string(pointTotal));
//}
//
//void UIPanel::render(sf::RenderWindow& window) const {
//	window.draw(textTotal);
//}
