#include <SFML/Graphics.hpp>
//#include "pacman.h"
#include "Game.h"

void handleEvents(sf::RenderWindow& window) {
	sf::Event event;
	while (window.pollEvent(event))
	{

		if (event.type == sf::Event::Closed)
			window.close();
	}
}

void update(sf::Clock& clock, Game& game)
{
	const float elapsedTime = clock.getElapsedTime().asSeconds();
	clock.restart();
	game.updateGame(elapsedTime);
}

void render(sf::RenderWindow& window, const Game& game)
{
	window.clear();
	game.render(window);
	window.display();
}


int main()
{
	sf::RenderWindow window(sf::VideoMode(window_length, window_width), " Pacman by Alexey Egorov");
	window.setFramerateLimit(60);
	sf::Clock clock;
	Game game;
	//game.createMaze();
	while (window.isOpen())
	{
		handleEvents(window);
		update(clock, game);
		render(window, game);
	}
	return 0;
}
