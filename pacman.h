#pragma once
#include "Cell.h"

//enum struct Collisions
//{
//    NONE,
//    WALL,
//    GHOST,
//    GUM
//};




static const sf::Color PACKMAN_COLOR = sf::Color(255, 216, 0);
static const float PACKMAN_SPEED = 120.f; // pixels per second.
static const float PACKMAN_RADIUS = 16.f; // pixels

class Pacman : public MovingEntity
{
public:
    Pacman(int xPos, int yPos, int size);

    void updatePacmanDirection();

    sf::Vector2f updatePacman(float elapsedTime);
    //Collisions isCollision(Pacman& pacman, sf::Vector2f& movement);

};
